


$(document).ready(function(){
  if ( window.location.hash ) scroll(0,0);
  setTimeout( function() { scroll(0,0); }, 1);
  
  $('#header').css({
      'position' : 'absolute',
      'left' : '40%',
      'top' : '40%',
  });

  $('#portfolio').on('click', function(event) { 
    setTimeout(function(){
      $('#landing-page-content-dropdown').toggle('show');
      if ($('#portfolio').html() == "PORTFOLIO") {
        $('#portfolio').html("HIDE PORTFOLIO");
        $('#landing-page-content').css("margin-bottom", "0px");
      } else {
        $('#portfolio').html("PORTFOLIO");
        $('#landing-page-content').css("margin-bottom", "60px");
      }
    }, 600);        
  });
 
  $('#portfolio').on('click', function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top-100
    }, 700, 'swing');
    return false;
  });
});





